# i3
## config
~/.config/i3/config

## bumblebee-status
you need fontawesome for bumblebee-status

## urxvt
in __.Xdefaults__

## gtk looking good
### gtk 2
For GTK+ 2.x, __~/.gtkrc-2.0__. For instance:

```
gtk-theme-name = "Adwaita"
gtk-font-name = "Sans 9"
```

### gtk 3
For GTK+ 3.x, __~/.config/gtk-3.0/settings.ini__:

```
[Settings]
gtk-theme-name=Adwaita
gtk-icon-theme-name=Adwaita
gtk-font-name=Sans 9
gtk-cursor-theme-size=0
gtk-toolbar-style=GTK_TOOLBAR_TEXT
gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
gtk-button-images=0
gtk-menu-images=1
gtk-enable-event-sounds=1
gtk-enable-input-feedback-sounds=1
gtk-xft-antialias=1
gtk-xft-hinting=1
gtk-xft-hintstyle=hintfull
gtk-xft-rgba=rgb
gtk-cursor-theme-name=Adwaita
```
